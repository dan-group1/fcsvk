import { NavLink } from 'react-router-dom';
import './style.css';

const Player = ({ title, img, index }) => {
	return (
		<NavLink to={`/player/${index}`}>
			<li className="player">
				<img src={img} alt={title} className="player__img" />
				<h3 className="player__title">{title}</h3>
			</li>
		</NavLink>
	);
};

export default Player;
