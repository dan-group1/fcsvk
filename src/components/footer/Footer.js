import "./style.css";

import vk from '../../img/icons/vk.svg';
import instagram from '../../img/icons/instagram.svg';
import twitter from '../../img/icons/twitter.svg';
import telegram from '../../img/icons/telegram.svg'; 
import gitHub from '../../img/icons/gitHub.svg';

const Footer = () => {
    return (
		<footer className="footer">
			<div className="container">
				<div className="footer__wrapper">
					<ul className="social">
						<li className="social__item">
							<a href="https://vk.com/svkacademy">
								<img src={vk} alt="Link" />
							</a>
						</li>
						<li className="social__item">
							<a href="https://web.telegram.org/a/#-1748672868">
								<img src={telegram} alt="Link" />
							</a>
						</li>
						<li className="social__item">
							<a href="https://gitlab.com/dan-group1/fcsvk">
								<img src={gitHub} alt="Link" />
							</a>
						</li>
					</ul>
					<div className="copyright">
						<p>© 2022 fсsvk.ru</p>
					</div>
				</div>
			</div>
		</footer>
	);
}

export default Footer;