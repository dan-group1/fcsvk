import "./style.css";
import svk from "../../img/command.png";

const Header = () => {
    return (
		<header className="header">
			<div className="header__wrapper">
				<h1 className="header__title">
					<strong>
						Добро пожаловать на сайт  <em> СВК</em>
					</strong>
				</h1>
				<div className="header__text">
					<p>Мы любительская футбольная команда!</p>
					<p>Мы за красивую игру! Мы за результат! Мы за удовольствие от футбола!</p>
				</div>
				<img src={svk} alt="SVK"/>
				<br/>
				<br/>
				{/* <a href="#!" className="btn">
					Зайти к нам
				</a> */}
			</div>
		</header>
	);
}

export default Header;