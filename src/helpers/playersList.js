import player01 from "../img/players/01.jpg";
import player01Big from "../img/players/01-big.jpg";

import player02 from "../img/players/02.jpg";
import player02Big from "../img/players/02-big.jpg";

import player03 from "../img/players/03.jpg";
import player03Big from "../img/players/03-big.jpg";

import player04 from "../img/players/04.jpg";
import player04Big from "../img/players/04-big.jpg";

import player05 from "../img/players/05.jpg";
import player05Big from "../img/players/05-big.jpg";

import player06 from "../img/players/06.jpg";
import player06Big from "../img/players/06-big.jpg";

const players = [
	{
		title: 'Илья Смирнов',
		skills: 'Защитник, капитан',
		img: player01,
		imgBig: player01Big,
		gitHubLink: 'https://github.com',
	},
	{
		title: 'Виктор Тарусин',
		img: player02,
		imgBig: player02Big,
		skills: 'React, PHP, MySql',
		gitHubLink: 'https://github.com',
	},
	{
		title: 'Илья Галибин',
		img: player03,
		imgBig: player03Big,
		skills: 'Vue JS, Node.js, MongoDB',
		gitHubLink: 'https://github.com',
	},
	{
		title: 'Бабек',
		img: player04,
		imgBig: player04Big,
		skills: 'React Native',
	},
	{
		title: 'Landing',
		img: player05,
		imgBig: player05Big,
		skills: 'HTML, SCSS, JS',
	},
	{
		title: 'Gaming community',
		img: player06,
		imgBig: player06Big, 
		skills: 'React, PHP, MySql',
	},
];

export {players}