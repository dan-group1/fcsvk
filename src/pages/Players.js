import Player from '../components/player/Player';
import {players} from "../helpers/playersList"

const Players = () => {
	return (
		<main className="section">
			<div className="container">
				<h2 className="title-1">Игроки</h2>
				<ul className="players">
					{players.map((player, index) => {
						return (
							<Player
								key={index}
								title={player.title}
								img={player.img}
								index={index}
							/>
						);
					})}
				</ul>
			</div>
		</main>
	);
};

export default Players;
