import {useParams} from "react-router-dom";
import BtnGitHub from "../components/btnGitHub/BtnGitHub";
import {players} from "../helpers/playersList"

const Player = () => {
	const {id} = useParams();
	const player = players[id];

    return (
		<main className="section">
			<div className="container">
				<div className="player-details">
					<h1 className="title-1">{player.title}</h1>

					<img
						src={player.imgBig}
						alt={player.title}
						className="player-details__cover"
					/>

					<div className="player-details__desc">
						<p>Skills: {player.skills}</p>
					</div>

					{player.gitHubLink && (
						<BtnGitHub link="https://github.com" />
					)}
				</div>
			</div>
		</main>
	);
}

export default Player;