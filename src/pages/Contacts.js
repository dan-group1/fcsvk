const Contacts = () => {
    return (
		<main className="section">
			<div className="container">
				<h1 className="title-1">Контакты</h1>


				<ul className="content-list">

				<li className="content-list__item">
						<h2 className="title-2">Telegram / WhatsApp</h2>
						<p>
							<a href="tel:+79683384488">+7(968)338-44-88</a>
						</p>
					</li>

					<li className="content-list__item">
						<h2 className="title-2">Адрес стадиона</h2>
						<p>Лодочная ул., 33, Москва</p>
					</li>
					{/* <li className="content-list__item">
						<h2 className="title-2">Email</h2>
						<p>
							<a href="mailto:webdev@protonmail.com">
								webdev@protonmail.com
							</a>
						</p>
					</li> */}
				</ul>
			</div>
		</main>
	);
}

export default Contacts;