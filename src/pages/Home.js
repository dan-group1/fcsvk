import Header from '../components/header/Header'

const Home = () => {
    return (
		<>
			<Header />
			<main className="section">
				<div className="container">
					<ul className="content-list">
						<li className="content-list__item">
							<h2 className="title-2">Кто мы такие?</h2>
							<p>
							Мы молодая и амбициозная команда, которая ищет игроков в свой дружный коллектив!
Мы участвуем в ЛФЛ Север 3 Дивизион и в Лиге Ф по выходным!

Тренировки во вторник и четверг с квалифицированным тренером.)
							</p>
						</li>
						<li className="content-list__item">
							<h2 className="title-2">Почему мы?</h2>
							<p>Ты любишь футбол и занимался им во дворе или в ДЮСШ, а может быть даже играл в академии профессионального клуба?
Окунись в приятную атмосферу тренировок, игр и хорошего времяпрепровождения.</p>
						</li>
					</ul>
				</div>
			</main>
		</>
	);
}

export default Home;